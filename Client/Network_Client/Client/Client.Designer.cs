﻿namespace Client
{
    partial class Client
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.connectButton = new System.Windows.Forms.Button();
            this.ipTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.usernameTextBox = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.chatReceiveWindowTextBox = new System.Windows.Forms.RichTextBox();
            this.portTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chatSendBox = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.sendButton = new System.Windows.Forms.Button();
            this.disconnectButton = new System.Windows.Forms.Button();
            this.logTextBox = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.organizationInfoTextBox = new System.Windows.Forms.RichTextBox();
            this.createEventButton = new System.Windows.Forms.Button();
            this.eventsCombobox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.participateButton = new System.Windows.Forms.Button();
            this.rejectButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.eventDetailsBox = new System.Windows.Forms.RichTextBox();
            this.getAllEventsButton = new System.Windows.Forms.Button();
            this.onlineComboBox = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.buddyInvitationsComboBox = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.addToBuddiesBtn = new System.Windows.Forms.Button();
            this.acceptBuddyBtn = new System.Windows.Forms.Button();
            this.rejectBuddyBtn = new System.Windows.Forms.Button();
            this.currentBuddiesTextBox = new System.Windows.Forms.RichTextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(220, 52);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 23);
            this.connectButton.TabIndex = 0;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // ipTextBox
            // 
            this.ipTextBox.Location = new System.Drawing.Point(70, 36);
            this.ipTextBox.Name = "ipTextBox";
            this.ipTextBox.Size = new System.Drawing.Size(144, 20);
            this.ipTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "IP:";
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.Location = new System.Drawing.Point(70, 88);
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.Size = new System.Drawing.Size(144, 20);
            this.usernameTextBox.TabIndex = 3;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(6, 91);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(58, 13);
            this.label.TabIndex = 4;
            this.label.Text = "Username:";
            // 
            // chatReceiveWindowTextBox
            // 
            this.chatReceiveWindowTextBox.Location = new System.Drawing.Point(38, 277);
            this.chatReceiveWindowTextBox.Name = "chatReceiveWindowTextBox";
            this.chatReceiveWindowTextBox.ReadOnly = true;
            this.chatReceiveWindowTextBox.Size = new System.Drawing.Size(257, 74);
            this.chatReceiveWindowTextBox.TabIndex = 5;
            this.chatReceiveWindowTextBox.Text = "";
            // 
            // portTextBox
            // 
            this.portTextBox.Location = new System.Drawing.Point(70, 62);
            this.portTextBox.Name = "portTextBox";
            this.portTextBox.Size = new System.Drawing.Size(144, 20);
            this.portTextBox.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Port:";
            // 
            // chatSendBox
            // 
            this.chatSendBox.Location = new System.Drawing.Point(369, 69);
            this.chatSendBox.Name = "chatSendBox";
            this.chatSendBox.Size = new System.Drawing.Size(150, 35);
            this.chatSendBox.TabIndex = 4;
            this.chatSendBox.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(366, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Chat Send:";
            // 
            // sendButton
            // 
            this.sendButton.Location = new System.Drawing.Point(527, 69);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(75, 23);
            this.sendButton.TabIndex = 10;
            this.sendButton.Text = "Send";
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.sendButton_Click);
            // 
            // disconnectButton
            // 
            this.disconnectButton.Location = new System.Drawing.Point(220, 81);
            this.disconnectButton.Name = "disconnectButton";
            this.disconnectButton.Size = new System.Drawing.Size(75, 23);
            this.disconnectButton.TabIndex = 11;
            this.disconnectButton.Text = "Disconnect";
            this.disconnectButton.UseVisualStyleBackColor = true;
            this.disconnectButton.Click += new System.EventHandler(this.disconnectButton_Click);
            // 
            // logTextBox
            // 
            this.logTextBox.Location = new System.Drawing.Point(36, 142);
            this.logTextBox.Name = "logTextBox";
            this.logTextBox.ReadOnly = true;
            this.logTextBox.Size = new System.Drawing.Size(256, 93);
            this.logTextBox.TabIndex = 12;
            this.logTextBox.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(44, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Log:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 252);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Chat:";
            // 
            // label6
            // 
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(-3, 111);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(620, 2);
            this.label6.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label7.Location = new System.Drawing.Point(24, 358);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(620, 2);
            this.label7.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(26, 387);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Create Event:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(26, 411);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "Information:";
            // 
            // organizationInfoTextBox
            // 
            this.organizationInfoTextBox.Location = new System.Drawing.Point(29, 439);
            this.organizationInfoTextBox.Name = "organizationInfoTextBox";
            this.organizationInfoTextBox.Size = new System.Drawing.Size(162, 150);
            this.organizationInfoTextBox.TabIndex = 21;
            this.organizationInfoTextBox.Text = "";
            // 
            // createEventButton
            // 
            this.createEventButton.Location = new System.Drawing.Point(32, 600);
            this.createEventButton.Name = "createEventButton";
            this.createEventButton.Size = new System.Drawing.Size(75, 23);
            this.createEventButton.TabIndex = 22;
            this.createEventButton.Text = "Create";
            this.createEventButton.UseVisualStyleBackColor = true;
            this.createEventButton.Click += new System.EventHandler(this.createEventButton_Click);
            // 
            // eventsCombobox
            // 
            this.eventsCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.eventsCombobox.FormattingEnabled = true;
            this.eventsCombobox.Location = new System.Drawing.Point(264, 384);
            this.eventsCombobox.Name = "eventsCombobox";
            this.eventsCombobox.Size = new System.Drawing.Size(121, 21);
            this.eventsCombobox.TabIndex = 23;
            this.eventsCombobox.SelectedIndexChanged += new System.EventHandler(this.eventsCombobox_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(262, 368);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Available Events :";
            // 
            // participateButton
            // 
            this.participateButton.Location = new System.Drawing.Point(403, 382);
            this.participateButton.Name = "participateButton";
            this.participateButton.Size = new System.Drawing.Size(75, 23);
            this.participateButton.TabIndex = 25;
            this.participateButton.Text = "Participate";
            this.participateButton.UseVisualStyleBackColor = true;
            this.participateButton.Click += new System.EventHandler(this.participateButton_Click);
            // 
            // rejectButton
            // 
            this.rejectButton.Location = new System.Drawing.Point(483, 382);
            this.rejectButton.Name = "rejectButton";
            this.rejectButton.Size = new System.Drawing.Size(75, 23);
            this.rejectButton.TabIndex = 26;
            this.rejectButton.Text = "Reject";
            this.rejectButton.UseVisualStyleBackColor = true;
            this.rejectButton.Click += new System.EventHandler(this.rejectButton_Click);
            // 
            // updateButton
            // 
            this.updateButton.Location = new System.Drawing.Point(564, 382);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(75, 23);
            this.updateButton.TabIndex = 27;
            this.updateButton.Text = "Update";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(261, 439);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(93, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "Event Information:";
            // 
            // eventDetailsBox
            // 
            this.eventDetailsBox.Location = new System.Drawing.Point(261, 464);
            this.eventDetailsBox.Name = "eventDetailsBox";
            this.eventDetailsBox.Size = new System.Drawing.Size(383, 125);
            this.eventDetailsBox.TabIndex = 29;
            this.eventDetailsBox.Text = "";
            // 
            // getAllEventsButton
            // 
            this.getAllEventsButton.Location = new System.Drawing.Point(524, 411);
            this.getAllEventsButton.Name = "getAllEventsButton";
            this.getAllEventsButton.Size = new System.Drawing.Size(115, 23);
            this.getAllEventsButton.TabIndex = 30;
            this.getAllEventsButton.Text = "Get All Events";
            this.getAllEventsButton.UseVisualStyleBackColor = true;
            this.getAllEventsButton.Click += new System.EventHandler(this.getAllEventsButton_Click);
            // 
            // onlineComboBox
            // 
            this.onlineComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.onlineComboBox.FormattingEnabled = true;
            this.onlineComboBox.Location = new System.Drawing.Point(332, 172);
            this.onlineComboBox.Name = "onlineComboBox";
            this.onlineComboBox.Size = new System.Drawing.Size(121, 21);
            this.onlineComboBox.TabIndex = 31;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(327, 147);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 13);
            this.label12.TabIndex = 32;
            this.label12.Text = "Online Users:";
            // 
            // buddyInvitationsComboBox
            // 
            this.buddyInvitationsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.buddyInvitationsComboBox.FormattingEnabled = true;
            this.buddyInvitationsComboBox.Location = new System.Drawing.Point(476, 172);
            this.buddyInvitationsComboBox.Name = "buddyInvitationsComboBox";
            this.buddyInvitationsComboBox.Size = new System.Drawing.Size(121, 21);
            this.buddyInvitationsComboBox.TabIndex = 33;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(473, 147);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(143, 13);
            this.label19.TabIndex = 34;
            this.label19.Text = "Received Buddy Invitations :";
            // 
            // addToBuddiesBtn
            // 
            this.addToBuddiesBtn.Location = new System.Drawing.Point(332, 199);
            this.addToBuddiesBtn.Name = "addToBuddiesBtn";
            this.addToBuddiesBtn.Size = new System.Drawing.Size(75, 23);
            this.addToBuddiesBtn.TabIndex = 35;
            this.addToBuddiesBtn.Text = "Add";
            this.addToBuddiesBtn.UseVisualStyleBackColor = true;
            this.addToBuddiesBtn.Click += new System.EventHandler(this.addToBuddiesBtn_Click);
            // 
            // acceptBuddyBtn
            // 
            this.acceptBuddyBtn.Location = new System.Drawing.Point(476, 199);
            this.acceptBuddyBtn.Name = "acceptBuddyBtn";
            this.acceptBuddyBtn.Size = new System.Drawing.Size(75, 23);
            this.acceptBuddyBtn.TabIndex = 36;
            this.acceptBuddyBtn.Text = "Accept";
            this.acceptBuddyBtn.UseVisualStyleBackColor = true;
            this.acceptBuddyBtn.Click += new System.EventHandler(this.acceptBuddyBtn_Click);
            // 
            // rejectBuddyBtn
            // 
            this.rejectBuddyBtn.Location = new System.Drawing.Point(553, 199);
            this.rejectBuddyBtn.Name = "rejectBuddyBtn";
            this.rejectBuddyBtn.Size = new System.Drawing.Size(75, 23);
            this.rejectBuddyBtn.TabIndex = 37;
            this.rejectBuddyBtn.Text = "Reject";
            this.rejectBuddyBtn.UseVisualStyleBackColor = true;
            this.rejectBuddyBtn.Click += new System.EventHandler(this.rejectBuddyBtn_Click);
            // 
            // currentBuddiesTextBox
            // 
            this.currentBuddiesTextBox.Location = new System.Drawing.Point(340, 249);
            this.currentBuddiesTextBox.Name = "currentBuddiesTextBox";
            this.currentBuddiesTextBox.Size = new System.Drawing.Size(277, 96);
            this.currentBuddiesTextBox.TabIndex = 38;
            this.currentBuddiesTextBox.Text = "";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(337, 233);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(84, 13);
            this.label52.TabIndex = 39;
            this.label52.Text = "current Buddies:";
            // 
            // Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(655, 629);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.currentBuddiesTextBox);
            this.Controls.Add(this.rejectBuddyBtn);
            this.Controls.Add(this.acceptBuddyBtn);
            this.Controls.Add(this.addToBuddiesBtn);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.buddyInvitationsComboBox);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.onlineComboBox);
            this.Controls.Add(this.getAllEventsButton);
            this.Controls.Add(this.eventDetailsBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.rejectButton);
            this.Controls.Add(this.participateButton);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.eventsCombobox);
            this.Controls.Add(this.createEventButton);
            this.Controls.Add(this.organizationInfoTextBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.logTextBox);
            this.Controls.Add(this.disconnectButton);
            this.Controls.Add(this.sendButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.chatSendBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.portTextBox);
            this.Controls.Add(this.chatReceiveWindowTextBox);
            this.Controls.Add(this.label);
            this.Controls.Add(this.usernameTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ipTextBox);
            this.Controls.Add(this.connectButton);
            this.Name = "Client";
            this.Text = "Client";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.TextBox ipTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox usernameTextBox;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.RichTextBox chatReceiveWindowTextBox;
        private System.Windows.Forms.TextBox portTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox chatSendBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.Button disconnectButton;
        private System.Windows.Forms.RichTextBox logTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RichTextBox organizationInfoTextBox;
        private System.Windows.Forms.Button createEventButton;
        private System.Windows.Forms.ComboBox eventsCombobox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button participateButton;
        private System.Windows.Forms.Button rejectButton;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RichTextBox eventDetailsBox;
        private System.Windows.Forms.Button getAllEventsButton;
        private System.Windows.Forms.ComboBox onlineComboBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox buddyInvitationsComboBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button addToBuddiesBtn;
        private System.Windows.Forms.Button acceptBuddyBtn;
        private System.Windows.Forms.Button rejectBuddyBtn;
        private System.Windows.Forms.RichTextBox currentBuddiesTextBox;
        private System.Windows.Forms.Label label52;
    }
}

