﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

namespace Server_Network
{
    public class ClientInformation
    {
        public String username { get; set; }
        public Socket socket { get; set; }

        public ClientInformation()
        {
        
        }
        public ClientInformation(String username,Socket socket)
        {
            this.username = username;
            this.socket = socket;
        }
        
    }
}
