﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Web.Script.Serialization;
using System.Collections;
namespace Server_Network
{

    public partial class Server : Form
    {
        public int eventCount = 0;
        public Socket listenerSocket;
        public List<ClientInformation> connectedClients { get; set; }
        public Boolean exceptionPrinted = false;
        public Dictionary<int, Organization> createdOrganizations;
        public Dictionary<String, HashSet<String>> buddyRelationships;
        public Server()
        {
            InitializeComponent();
            initializeTextBoxValues();
            Form.CheckForIllegalCrossThreadCalls = false;
            this.connectedClients = new List<ClientInformation>();
            this.createdOrganizations = new Dictionary<int, Organization>();
            this.buddyRelationships = new Dictionary<string, HashSet<string>>();
            this.listenerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        }
        /// <summary>
        /// Thread that listens for incoming connections when there is an incoming connection from a client
        /// it assigns a new thread to that user.
        /// </summary>
        public void listenForIncomingConnections()
        {
            while (true)
            {
                Socket newClient = this.listenerSocket.Accept();
                LogTextEvent(logTextBox, " A new client candidate has arrived .",false);
                Thread receiveThread = new Thread(new ParameterizedThreadStart(receiveData));
                receiveThread.IsBackground = true;
                receiveThread.Start(newClient);
            }
        }

        public void initializeTextBoxValues()
        {
            this.portTextBox.Text = "4242";
        }
        /// <summary>
        /// This method runs until disconnection. it receives data from the buffer and append
        /// it to a string until it encounters the seperator keyword after that it just sends
        /// the packet to handleData method
        /// </summary>
        /// <param name="currentSocket">Client Socket used in the thread</param>
        public void receiveData(object currentSocket)
        {
            Socket clientSocket = (Socket)currentSocket;
            int bufferSize = clientSocket.SendBufferSize;
            byte[] receivedBytes = new byte[bufferSize];
            int receivedByteCount = 0;
            String receivedStr = "";

            while (true)
            {
                try
                {
                    int read;
                    while ((read = clientSocket.Receive(receivedBytes)) > 0)
                    {
                        receivedStr += Encoding.Default.GetString(receivedBytes);
                        Array.Clear(receivedBytes, 0, receivedBytes.Length);
                        if (SimpleSerializer.dataIsComplete(receivedStr) == true)
                        {
                            break;
                        }
                    }

                }
                catch (SocketException ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.ToString());

                    removeClient(clientSocket);
                    sendOnlineUsers();
                    break;
                }
                if (receivedStr.Length > 0)
                {

                    String toSend = String.Copy(receivedStr);
                    Thread thread = new Thread(() => handleData(toSend, clientSocket));
                    thread.IsBackground = true;
                    thread.Start();
                }
                receivedStr = "";
            }

        }
        /// <summary>
        /// Handle data removes the delimiter and then deserializes the received JSON String into a DataWrapper,
        /// sends DataWrapper object to redirection
        /// </summary>
        /// <param name="receivedStr">The received Json data with the delimiter appended to it</param>
        public void handleData(object receivedStr, object socket)
        {
            String message = (String)receivedStr;
            Socket clientSocket = (Socket)socket;
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            String strippedJson = SimpleSerializer.getUntilCipher(message, ConfigurationSettings.getHash());
            DataWrapper receivedPacket = jsSerializer.Deserialize<DataWrapper>(strippedJson);
            redirectReceivedPacket(receivedPacket, clientSocket);


        }
        /// <summary>
        /// Simple redirecter, redirects according to the purpose of the packet
        /// </summary>
        /// <param name="receivedPacket"></param>
        /// <param name="clientSocket"></param>
        public void redirectReceivedPacket(DataWrapper receivedPacket, Socket clientSocket)
        {
            if (receivedPacket.purpose.Equals(Purpose.Auth))
            {
                String clientUsername = receivedPacket.sendername;
                if (isAUniqueClient(clientUsername) == true)
                {
                    authenticateUser(clientUsername, clientSocket);
                    sendOnlineUsers();
                    if (buddyRelationships.ContainsKey(clientUsername))
                    {
                        LogTextEvent(logTextBox, receivedPacket.sendername + " Sending old buddies ",false);
                        sendOldBuddies(clientUsername);
                    }
                    else
                    {
                        buddyRelationships.Add(clientUsername, new HashSet<string>());
                    }
                }
                else
                {
                    LogTextEvent(logTextBox, "Same User tried to join to the server, NOT Allowed",false);
                    sendConnectionTermination("You have a duplicate username ", clientSocket);

                }
            }
            else if (receivedPacket.recipient.Equals("Broadcast"))
            {
                if (receivedPacket.purpose.Equals(Purpose.TextMessage))
                {
                    LogTextEvent(logTextBox, " BROADCASTING : " + receivedPacket.sendername + " >: " + receivedPacket.Text,false);
                    broadcast(receivedPacket);
                }
                else if (receivedPacket.purpose.Equals(Purpose.CreateOrganization))
                {
                    LogTextEvent(logTextBox, receivedPacket.sendername + " Created organization " + receivedPacket.organization.information,false);
                    receivedPacket.organization.id = eventCount;
                    this.createdOrganizations.Add(eventCount, receivedPacket.organization);
                    eventCount++;
                    notifyUsersAboutCreatedEvent(receivedPacket);

                }
            }
            else if (receivedPacket.purpose.Equals(Purpose.AcceptOrganization))
            {
                LogTextEvent(logTextBox, receivedPacket.sendername + " Accepts organization " + receivedPacket.organization.information,false);
                String sender = receivedPacket.sendername;
                int organizationID = Convert.ToInt32(receivedPacket.Text);
                Organization organization = getOrganization(organizationID);
                organization.updateParticipantStatus(sender, Status.Accepted);
                sendUpdatedOrganizationInfoToUser(organization, sender);
                if (!(organization.sender.Equals(sender)))
                {
                    sendUpdatedOrganizationInfoToUser(organization, organization.sender);
                }
            }
            else if (receivedPacket.purpose.Equals(Purpose.RejectOrganization))
            {
                LogTextEvent(logTextBox, receivedPacket.sendername + " Rejects organization " + receivedPacket.organization.information,false);
                String sender = receivedPacket.sendername;
                int organizationID = Convert.ToInt32(receivedPacket.Text);
                Organization organization = getOrganization(organizationID);
                organization.updateParticipantStatus(sender, Status.Rejected);
                sendUpdatedOrganizationInfoToUser(organization, sender);
                if (!(organization.sender.Equals(sender)))
                {
                    sendUpdatedOrganizationInfoToUser(organization, organization.sender);
                }

            }
            else if (receivedPacket.purpose.Equals(Purpose.WantUpdate))
            {
                LogTextEvent(logTextBox, receivedPacket.sendername + " Wants update on " + receivedPacket.Text,false);
                String sender = receivedPacket.sendername;
                int organizationID = Convert.ToInt32(receivedPacket.Text);
                Organization organization = createdOrganizations[organizationID];
                sendUpdatedOrganizationInfoToUser(organization, sender);
            }
            else if (receivedPacket.purpose.Equals(Purpose.BatchUpdate))
            {
                LogTextEvent(logTextBox , " BEGIN Batch update request from " + receivedPacket.sendername  + " to batch Update",false);
                DataWrapper batchData = new DataWrapper();

                HashSet<String> customPacketCandidates = new HashSet<String>();
                customPacketCandidates.Add(receivedPacket.sendername);
                foreach (KeyValuePair<int, Organization> entry in this.createdOrganizations)
                {
                    Organization organization = (Organization)entry.Value;
                    String sender = receivedPacket.sendername;
                    String organizator = organization.sender;
                    
                    bool senderAlreadyExist = organization.participantExist(sender);
                    
                    if( (areTheyBuddies(sender,organizator)==true || organizator.Equals(sender)) ) 
                     {

                         addAllNewParticipants(organizator, organization,customPacketCandidates);
                         if (customPacketCandidates.Contains(organizator) == false && !senderAlreadyExist)
                         {
                             customPacketCandidates.Add(organizator);
                         }
                        
                     }
                    foreach(String candidate in customPacketCandidates)
                    {
                        
                            DataWrapper customPacket=createCustomTailoredPacket(candidate);
                            sendAllOrganizations(customPacket, candidate);
                     }
               }
                LogTextEvent(logTextBox, "END batchUpdate request from " + receivedPacket.sendername,false);
            }
            else if (receivedPacket.purpose.Equals(Purpose.WantToBeBuddies))
            {
                LogTextEvent(logTextBox, receivedPacket.sendername + " wants to be buddies with " + receivedPacket.recipient,false);
                receivedPacket.purpose = Purpose.BuddyRequest;
                Socket socket = getSocketFromClientName(receivedPacket.recipient);
                sendDataToDestination(receivedPacket, socket);
                
            }
            else if(receivedPacket.purpose.Equals(Purpose.YepWantToBeBuddies))
            {
                LogTextEvent(logTextBox, receivedPacket.sendername + " accepted to be buddies with " + receivedPacket.recipient,false);
                String sender = receivedPacket.sendername;
                String recipient = receivedPacket.recipient;
                makeUsersBuddies(sender, recipient);
                sendThemBuddyAcceptance(sender, recipient);
                
            }
        }

        /// <summary>
        /// Basically a packet that contains the updated information about events
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public DataWrapper createCustomTailoredPacket(String username)
        {
            DataWrapper customDataWrapper = new DataWrapper();
            customDataWrapper.recipient=username;
            customDataWrapper.sendername="Server";
            customDataWrapper.purpose = Purpose.NotificationBatchUpdate;

            foreach (KeyValuePair<int, Organization> entry in this.createdOrganizations)
            {
                if (areTheyBuddies(entry.Value.sender, username) == true || username.Equals(entry.Value.sender))
                {
                    customDataWrapper.organizations.Add(entry.Value);
                }
            }
            printCustomPacket(customDataWrapper);
            return customDataWrapper;
        }
        public void addAllNewParticipants(String sender, Organization organization, HashSet<String> customPacketCandidates)
        {
            foreach ( String user in buddyRelationships[sender])
            {
                if (organization.participantExist(user) == false)
                {
                    organization.addParticipantIfDoesNotExist(user);
                }
                
            }
        }
        //Helper methods to print 
        public void printAllOrganizationsInTheServer()
        {
            LogTextEvent(logTextBox, "BEGIN Printing All organizations in the server",false);
            foreach (KeyValuePair<int, Organization> entry in this.createdOrganizations)
            {
                printOrganization(entry.Value);
            }

            LogTextEvent(logTextBox, "END Printing All organizations in the server",false);
        }
        public void printOrganization(Organization organization)
        {
            LogTextEvent(logTextBox, organization.information + " : ", true);
            LogTextEvent(logTextBox, "|| ", true);
            foreach ( Participant participant in organization.participants)
            {
                LogTextEvent(logTextBox, participant.name,true);
                LogTextEvent(logTextBox, ", ", true);
            }
            LogTextEvent(logTextBox, " ", false);
        }
        public void printAllOrganizationsInThePacket(DataWrapper data)
        {
            LogTextEvent(logTextBox, "BEGIN Printing All Events in packet", false);
            LogTextEvent(logTextBox, "Printing All The Events on the data with the following properties:", false);
            LogTextEvent(logTextBox, "RECIPIENT : " + data.recipient, false);
            foreach(Organization organization in data.organizations)
            {
                printOrganization(organization);
            }
            LogTextEvent(logTextBox, "END Printing All Events in packet", false);

        }

        public void printCustomPacket(DataWrapper packet)
        {
            LogTextEvent(logTextBox, "Custom Packet to " + packet.recipient, false);
            LogTextEvent(logTextBox, "Organizations:", false);
            foreach(Organization organization in packet.organizations)
            {
                printOrganization(organization);
            }
        }

        /// <summary>
        /// Server creates a buddy acceptance packet in the case that they both agree to be buddies
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="recipient"></param>
        public void sendThemBuddyAcceptance(String sender,String recipient)
        {
            DataWrapper dataWrapper = new DataWrapper();
            dataWrapper.sendername = sender;
            dataWrapper.recipient = recipient;
            dataWrapper.Text = " Yes i want to be buddies";
            dataWrapper.purpose = Purpose.YepWantToBeBuddies;
            Socket recipientSocket = getSocketFromClientName(recipient);
            sendDataToDestination(dataWrapper,recipientSocket);
            dataWrapper.recipient = sender;
            dataWrapper.sendername = recipient;
            Socket senderSocket = getSocketFromClientName(sender);
            sendDataToDestination(dataWrapper, senderSocket);

        }
        /// <summary>
        /// Make them buddies , If a1 is buddy with a2 then a2 is also buddy with a1
        /// </summary>
        /// <param name="user1"></param>
        /// <param name="user2"></param>
        public void makeUsersBuddies(String user1,String user2)
        {
            if(buddyRelationships[user1].Contains(user2)==false)
            {
                LogTextEvent(logTextBox, user1 + " and  " + user2 + " are buddies", false);
                buddyRelationships[user1].Add(user2);
            }
            if (buddyRelationships[user2].Contains(user1) == false)
            {
                buddyRelationships[user2].Add(user1);
            }
        }
       
        /// <summary>
        /// Send the organization update to the user
        /// </summary>
        /// <param name="organization"></param>
        /// <param name="username"></param>
        public void sendUpdatedOrganizationInfoToUser(Organization organization, String username )
        {
            DataWrapper dataWrapper = new DataWrapper("Updating the organization", "Server", Purpose.NotifyOrganizationUpdate, username, organization);
            Socket destination = getSocketFromClientName(username);
            if (destination != null)
            {
                sendDataToDestination(dataWrapper, destination);
            }
        }
        /// <summary>
        /// Updates the user with sending a packet with NotificationBatchUpdate as flag , this method is not used
        /// </summary>
        /// <param name="organization"></param>
        /// <param name="username"></param>
        public void sendUpdatedBatchOrganizationInfoToUser(Organization organization, String username )
        {
            DataWrapper dataWrapper = new DataWrapper("Updating the organization", "Server", Purpose.NotificationBatchUpdate, username, organization);
            Socket destination = getSocketFromClientName(username);
            if (destination != null)
            {
                sendDataToDestination(dataWrapper, destination);
            }
        }
        /// <summary>
        /// Sends the information of all organizations to the user.
        /// </summary>
        /// <param name="dataWrapper"></param>
        /// <param name="username"></param>
        public void sendAllOrganizations(DataWrapper dataWrapper ,String username)
         {
             Socket destination = getSocketFromClientName(username);
             if (destination != null)
             {
                 sendDataToDestination(dataWrapper, destination);
             }
         }

        /// <summary>
        /// Utility method, sends BAN packet to the client
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="socketConnection"></param>
        public void sendConnectionTermination(String reason, Socket socketConnection)
        {
            DataWrapper dataWrapper = new DataWrapper(reason, "server", Purpose.Ban, "Not important");
            sendDataToDestination(dataWrapper, socketConnection);

        }
        /// <summary>
        /// //Sends a packet notifying the  buddies of the newly created event/organization 
        /// </summary>
        /// <param name="dataWrapper"></param>
        public void notifyUsersAboutCreatedEvent(DataWrapper dataWrapper)
        {
            dataWrapper.purpose = Purpose.NotifyNewCreatedOrganization;
            foreach (ClientInformation client in this.connectedClients)
            {
                if(areTheyBuddies(client.username,dataWrapper.organization.sender)==true)
                { 
                    dataWrapper.organization.addParticipantIfDoesNotExist(client.username);
                }
            }
            foreach (ClientInformation client in this.connectedClients)
            {
                if(areTheyBuddies(client.username,dataWrapper.organization.sender)==true || client.username.Equals(dataWrapper.organization.sender))
                {
                    LogTextEvent(logTextBox, "FORWARDING TO " + client.username, false);
                    sendDataToDestination(dataWrapper, client.socket);
                }
            }
        }

        public Organization getOrganization(int id)
        {
            return createdOrganizations[id];
        }

        public Socket getSocketFromClientName(String username)
        {
            foreach (ClientInformation clientInfo in connectedClients)
            {
                if (clientInfo.username.Equals(username))
                {
                    return clientInfo.socket;
                }
            }
            return null;
        }

        //Broadcast data to buddies
        public void broadcast(DataWrapper broadcastData)
        {
            foreach (ClientInformation clientInfo in connectedClients)
            {
                if (clientInfo.username != broadcastData.sendername)
                {
                    if (areTheyBuddies(broadcastData.sendername, clientInfo.username) == true && isUserOnline(clientInfo.username) == true)
                    {

                        LogTextEvent(logTextBox, " FORWARDING TO " + clientInfo.username, false);
                        sendDataToDestination(broadcastData, clientInfo.socket);
                    }
                }
            }
        }
        /// <summary>
        /// Simple wrapper
        /// </summary>
        /// <param name="dataToSend">Data wrapper to send</param>
        /// <param name="destination">destination socket</param>
        public void sendDataToDestination(DataWrapper dataToSend, Socket destination)
        {

            byte[] bytesToSend = SimpleSerializer.serializerIntoByteArray(dataToSend);
            Thread sendThread = new Thread(() => sendDataThroughSockets(destination, bytesToSend));
            sendThread.IsBackground = true;
            sendThread.Start();

        }

        /// <summary>
        /// Actually sends the data through socket
        /// </summary>
        /// <param name="destination">destination Socket</param>
        /// <param name="bytesToSend">Data to send in byte array representation</param>
        public void sendDataThroughSockets(Socket destination, byte[] bytesToSend)
        {
           int byteRemainingToSend = bytesToSend.Length;
            try
            {
                while (byteRemainingToSend > 0)
                {
                    byteRemainingToSend -= destination.Send(bytesToSend);
                }
            }
            catch (SocketException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }
        /// <summary>
        /// Authenticates the user by deciding if a client with a same name is already
        /// connected to the server
        /// </summary>
        /// <param name="username">username of the client</param>
        /// <param name="clientSocket">socket of the client</param>
        public void authenticateUser(String username, Socket clientSocket)
        {
            ClientInformation newClientInformation = new ClientInformation(username, clientSocket);
            String toPrint = username + " is joined,  Welcome " + newClientInformation.username;
            LogTextEvent(logTextBox, toPrint, false);

            connectedClients.Add(newClientInformation);
        }
        /// <summary>
        /// Removes client from the connectedClients
        /// </summary>
        /// <param name="socketToDisconnect"></param>
        public void removeClient(Socket socketToDisconnect)
        {
            for (int i = 0; i < this.connectedClients.Count; i++)
            {
                if (this.connectedClients[i].socket.Equals(socketToDisconnect))
                {
                    this.connectedClients[i].socket.Disconnect(false);
                    LogTextEvent(logTextBox, this.connectedClients[i].username + " has disconnected ", false);

                    this.connectedClients.RemoveAt(i);

                }
            }
        }
        /// <summary>
        /// Starts to listen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listenButton_Click(object sender, EventArgs e)
        {
            if (Validator.isValidPort(portTextBox.Text))
            {
                int port = Convert.ToInt32(portTextBox.Text);
                listenButton.Enabled = false;
                portTextBox.Enabled = false;
                logTextBox.AppendText("Listening from port " + portTextBox.Text + Environment.NewLine);
                
                IPEndPoint serverIp = new IPEndPoint(IPAddress.Any, port);
                this.listenerSocket.Bind(serverIp);
                this.listenerSocket.Listen(5);
                Thread listenThread = new Thread(listenForIncomingConnections);
                listenThread.IsBackground = true;
                listenThread.Start();
            }
            else
            {
                LogTextEvent(logTextBox, "Please enter a valid Port Number ", false);
            }
        }
        //Simple checks
        public Boolean isValidPort()
        {
            String portNumberStr = portTextBox.Text;
            int port;
            bool valid = false;
            if (int.TryParse(portNumberStr, out port))
            {
                if (port < 49151)
                {
                    valid = true;
                    return valid;
                }

            }
            return valid;
        }

        public Boolean areTheyBuddies(String user1, String user2)
        {
            if(buddyRelationships[user1].Contains(user2)==true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean isUserOnline ( String user)
        {
            foreach(ClientInformation client in connectedClients)
            {
                if(client.username.Equals(user))
                {
                    return true;
                }
            }
            return false;
        }

        public Boolean isAUniqueClient(String username)
        {
            foreach (ClientInformation clientInformation in this.connectedClients)
            {
                if (clientInformation.username.Equals(username))
                {
                    return false;
                }
            }
            return true;
        }
        //Sends all online users in the server
        public void sendOnlineUsers()
        {
            DataWrapper dataWrapper = new DataWrapper();
            dataWrapper.purpose = Purpose.sendingOnlineUsers;
            dataWrapper.Text = "Here update your online list";
            dataWrapper.sendername = "Server";
            foreach (ClientInformation client in this.connectedClients)
            {
                LogTextEvent(logTextBox, client.username + " is Online ", false);
                dataWrapper.users.Add(client.username);
            }
            //We have one dataWrapper now we can send it to everyone
            foreach (ClientInformation client in this.connectedClients)
            {
                dataWrapper.recipient = client.username;
                sendDataToDestination(dataWrapper, client.socket);
            }
        }
        //When user re enters with the same name it gets the old buddy list
        public void sendOldBuddies(String user)
        {
            DataWrapper dataWrapper = new DataWrapper("Hey I am sending you your old buddies","Server",Purpose.oldBuddyList,user,null);
            foreach ( String buddy in buddyRelationships[user])
            {
                dataWrapper.users.Add(buddy);
            }
            Socket socket = getSocketFromClientName(user);
            sendDataToDestination(dataWrapper,socket);
        }
        public Boolean isAReturnedUserWithBuddy(String user)
        {
            if(buddyRelationships.ContainsKey(user)==true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //A utility method i found from internet
        public void LogTextEvent(RichTextBox TextEventLog, string EventText , bool sameLine)
        {
            if (TextEventLog.InvokeRequired)
            {
                TextEventLog.BeginInvoke(new Action(delegate
                {
                    LogTextEvent(TextEventLog, EventText,sameLine);
                }));
                return;
            }

            string empty = "";



            // newline if first line, append if else.

            if (sameLine == true)
            {
                TextEventLog.AppendText(empty + EventText);
                TextEventLog.ScrollToCaret();
            }
            else if (TextEventLog.Lines.Length == 0 )
            {
                TextEventLog.AppendText(empty + EventText);
                TextEventLog.ScrollToCaret();
                TextEventLog.AppendText(System.Environment.NewLine);
            }
           
            else
            {
                TextEventLog.AppendText(empty + EventText + System.Environment.NewLine);
                TextEventLog.ScrollToCaret();
            }
        }



    }
}
