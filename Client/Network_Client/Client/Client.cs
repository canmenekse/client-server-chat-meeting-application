﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using System.Collections;


namespace Client
{
    public partial class Client : Form
    {
        public Socket clientSocket;
        public string username;
        public Boolean disconnect = false;
        public Boolean exceptionPrinted = false;
        public Dictionary<int, Organization> organizations = new Dictionary<int, Organization>();
        public HashSet<int> respondedInvitations = new HashSet<int>();
        public HashSet<String> buddies = new HashSet<String>();
        public int selectedOrganizationId = -1;

        public Client()
        {

            InitializeComponent();
            initializeTextBoxValues();
            sendButton.Enabled = false;
            chatSendBox.Enabled = false;
            disconnectButton.Enabled = false;
            createEventButton.Enabled = false;
            getAllEventsButton.Enabled = false;
            updateButton.Enabled = false;
            participateButton.Enabled = false;
            rejectButton.Enabled = false;
            this.clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Form.CheckForIllegalCrossThreadCalls = false;

        }

        public void initializeTextBoxValues()
        {
            this.ipTextBox.Text = "127.0.0.1";
            this.portTextBox.Text = "4242";
            this.usernameTextBox.Text = "Anybody";
        }
        /// <summary>
        /// connects to the given host with connectHost() and sends authentication packet to authenticate with the 
        /// host to avoid username duplication
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void connectButton_Click(object sender, EventArgs e)
        {


            Validator validator = new Validator();
            if (validator.validatedInputs(ipTextBox.Text, portTextBox.Text, usernameTextBox.Text) == true)
            {
                String ipAddress = ipTextBox.Text;
                int portNumber = Convert.ToInt32(portTextBox.Text);
                this.username = usernameTextBox.Text;
                connectButton.Enabled = false;

                Thread connectThread = new Thread(() => connectToHost(ipAddress, portNumber));
                connectThread.IsBackground = true;
                connectThread.Start();

            }
            else
            {
                String err = validator.generateTheErrString();
                logTextBox.AppendText(err);

            }

        }
        /// <summary>
        /// Handles the initial connection of the socket
        /// </summary>
        /// <param name="ipAddress">Host ip adress to Connect</param>
        /// <param name="portNumber">Port number to connect</param>
        public void connectToHost(String ipAddress, int portNumber)
        {
            try
            {

                this.Invoke((MethodInvoker)delegate
                {
                    ipTextBox.Enabled = false;
                    usernameTextBox.Enabled = false;
                    portTextBox.Enabled = false;
                });

                LogTextEvent(logTextBox, "Connecting to Server  ");

                //blocking
                this.clientSocket.Connect(ipAddress, portNumber);
                createAndRunReceiveThread();
                this.Invoke((MethodInvoker)delegate
                {
                    chatSendBox.Enabled = true;
                    sendButton.Enabled = true;
                    disconnectButton.Enabled = true;
                    createEventButton.Enabled = true;

                    getAllEventsButton.Enabled = true;
                });
                LogTextEvent(logTextBox, "Connected");




                sendData("I want to connect to the server ", Purpose.Auth, "Authentication");

            }
            catch (SocketException ex)
            {
                MessageBox.Show("Cannot connect to the Server ");
                this.Invoke((MethodInvoker)delegate
                {
                    ipTextBox.Enabled = true;
                    usernameTextBox.Enabled = true;
                    portTextBox.Enabled = true;
                    connectButton.Enabled = true;
                    sendButton.Enabled = false;
                    chatSendBox.Enabled = false;
                    disconnectButton.Enabled = false;
                });

            }

        }
        /// <summary>
        /// Helper method to create the receive thread
        /// </summary>
        public void createAndRunReceiveThread()
        {
            Thread receiveThread = new Thread(receiveData);
            receiveThread.IsBackground = true;
            receiveThread.Start();
        }
        /// <summary>
        /// This method runs until disconnection. it receives data from the buffer and append
        /// it to a string until it encounters the seperator keyword after that it just sends
        /// the packet to handleData method
        /// </summary>
        public void receiveData()
        {

            int bufferSize = this.clientSocket.SendBufferSize;
            byte[] receivedBytes = new byte[bufferSize];
            int receivedByteCount = 0;
            String receivedStr = "";
            while (disconnect == false)
            {
                try
                {

                    int read;
                    while ((read = this.clientSocket.Receive(receivedBytes)) > 0)
                    {
                        receivedStr += Encoding.Default.GetString(receivedBytes);
                        Array.Clear(receivedBytes, 0, receivedBytes.Length);
                        if (SimpleSerializer.dataIsComplete(receivedStr) == true)
                        {
                            break;
                        }
                    }

                }
                catch (SocketException ex)
                {
                    if (exceptionPrinted == false)
                    {
                        MessageBox.Show("Connection to the Host is disconnected");
                        exceptionPrinted = true;
                        this.Invoke((MethodInvoker)delegate
                        {
                            disconnectButton.PerformClick();
                        });

                    }
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                }
                if (receivedStr.Length > 0)
                {
                    String toSend = String.Copy(receivedStr);
                    handleData(toSend);
                }
                receivedStr = "";
            }

        }

        /// <summary>
        /// Adjusts GUI components
        /// </summary>
        public void enableOrganizationModify()
        {

            this.Invoke((MethodInvoker)delegate
            {
                updateButton.Enabled = true;
                participateButton.Enabled = true;
                rejectButton.Enabled = true;
            });

        }

        /// <summary>
        /// Handle data removes the delimiter and then deserializes the received JSON String into a DataWrapper,
        /// sends DataWrapper object to redirection
        /// </summary>
        /// <param name="receivedStr">The received Json data</param>
        public void handleData(object receivedStr)
        {
            String message = (String)receivedStr;
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            String strippedJson = SimpleSerializer.getUntilCipher(message, ConfigurationSettings.getHash());
            DataWrapper receivedPacket = jsSerializer.Deserialize<DataWrapper>(strippedJson);
            redirectReceivedPacket(receivedPacket, this.clientSocket);
        }
        /// <summary>
        /// Simple redirecter, redirects according to the purpose of the packet
        /// </summary>
        /// <param name="receivedPacket"></param>
        /// <param name="clientSocket"></param>
        public void redirectReceivedPacket(DataWrapper receivedPacket, Socket clientSocket)
        {
            if (receivedPacket.purpose.Equals(Purpose.TextMessage))
            {
                chatReceiveWindowTextBox.AppendText(receivedPacket.sendername + " >: " + receivedPacket.Text + Environment.NewLine);

            }
            else if (receivedPacket.purpose.Equals(Purpose.Ban))
            {

                this.Invoke((MethodInvoker)delegate
                {
                    sendButton.Enabled = false;
                    connectButton.Enabled = true;
                });

                MessageBox.Show(receivedPacket.Text);
                Application.Exit();
                this.clientSocket.Disconnect(true);

            }
            else if (receivedPacket.purpose.Equals(Purpose.NotifyNewCreatedOrganization))
            {

                this.Invoke((MethodInvoker)delegate
                {
                    this.eventDetailsBox.Clear();
                });

                Organization organization = receivedPacket.organization;

                organizations.Add(organization.id, organization);
                this.Invoke((MethodInvoker)delegate
                {
                    this.eventsCombobox.Items.Add(organization.id + "  " + organization.information);
                    this.eventDetailsBox.Text = "";
                    this.eventsCombobox.SelectedIndex = 0;
                });

                enableOrganizationModify();
            }
            else if (receivedPacket.purpose.Equals(Purpose.NotifyOrganizationUpdate))
            {


                enableOrganizationModify();
                this.Invoke((MethodInvoker)delegate
                {
                    this.eventDetailsBox.ResetText();
                });

                Organization updatedOrganization = receivedPacket.organization;
                this.Invoke((MethodInvoker)delegate
                {
                    LogTextEvent(logTextBox, "Received Update Event " + updatedOrganization.information);
                });
                int id = updatedOrganization.id;
                if (!(organizations.ContainsKey(id)))
                {
                    organizations.Add(updatedOrganization.id, updatedOrganization);
                    updateComboBoxCompletely();

                }
                else
                {

                    organizations[id] = updatedOrganization;
                    updateComboBox();
                }




            }
            //When we want information on all buddies events
            else if (receivedPacket.purpose.Equals(Purpose.NotificationBatchUpdate))
            {
                enableOrganizationModify();
                this.Invoke((MethodInvoker)delegate
                {
                    this.eventsCombobox.SelectedIndex = -1;
                    this.eventsCombobox.Items.Clear();
                    this.eventDetailsBox.ResetText();
                });
                organizations.Clear();
                List<Organization> organizationsReceived = receivedPacket.organizations;
                foreach (Organization organizationToAdd in organizationsReceived)
                {
                    organizations.Add(organizationToAdd.id, organizationToAdd);
                }


                updateComboBoxCompletely();
            }
            else if (receivedPacket.purpose.Equals(Purpose.sendingOnlineUsers))
            {
                this.Invoke((MethodInvoker)delegate
                {
                    this.onlineComboBox.SelectedIndex = -1;
                    this.onlineComboBox.Items.Clear();
                });


                foreach (String user in receivedPacket.users)
                {
                    if (user.Equals(this.username) == false)
                    {
                        this.Invoke((MethodInvoker)delegate
                        {
                            onlineComboBox.Items.Add(user);
                        });

                    }
                }

            }
            else if (receivedPacket.purpose.Equals(Purpose.BuddyRequest))
            {
                String buddyCandidate = receivedPacket.sendername;
                this.Invoke((MethodInvoker)delegate
                {
                    buddyInvitationsComboBox.Items.Add(buddyCandidate);
                });

            }
            else if (receivedPacket.purpose.Equals(Purpose.YepWantToBeBuddies))
            {
                String buddy = receivedPacket.sendername;
                if (buddies.Contains(buddy) == false)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        currentBuddiesTextBox.AppendText(buddy + Environment.NewLine);
                    });


                    buddies.Add(buddy);
                }
            }
            else if (receivedPacket.purpose.Equals(Purpose.oldBuddyList))
            {
                foreach (String buddy in receivedPacket.users)
                {

                    this.Invoke((MethodInvoker)delegate
                    {
                        currentBuddiesTextBox.AppendText(buddy + Environment.NewLine);
                    });
                    buddies.Add(buddy);
                }
            }

        }
        /// <summary>
        /// To print participants in the eventbox
        /// </summary>
        /// <param name="organization"></param>
        public void printParticipantsOfAnOrganization(Organization organization)
        {
            LogTextEvent(eventDetailsBox, "Participants : ");
            foreach (Participant participant in organization.participants)
            {
                LogTextEvent(eventDetailsBox, participant.name + " " + participant.status);

            }
        }

        /// <summary>
        /// A wrapper method to send data
        /// </summary>
        /// <param name="textData">content of the data</param>
        /// <param name="purpose">purpose of the data</param>
        /// <param name="recipient">recipient of the data</param>
        public void sendData(String textData, Purpose purpose, String recipient)
        {
            DataWrapper dataToSend = new DataWrapper(textData, this.username, purpose, recipient);
            byte[] bytesToSend = SimpleSerializer.serializerIntoByteArray(dataToSend);
            Thread sendThread = new Thread(new ParameterizedThreadStart(sendDataThroughSockets));
            sendThread.IsBackground = true;
            sendThread.Start(bytesToSend);

        }
        public void sendData(DataWrapper dataToSend)
        {
            byte[] bytesToSend = SimpleSerializer.serializerIntoByteArray(dataToSend);
            Thread sendThread = new Thread(new ParameterizedThreadStart(sendDataThroughSockets));
            sendThread.IsBackground = true;
            sendThread.Start(bytesToSend);
        }
        /// <summary>
        /// Actually sends the data through socket
        /// </summary>
        /// <param name="o">data to send</param>
        public void sendDataThroughSockets(object o)
        {
            byte[] bytesToSend = (byte[])o;
            int byteRemainingToSend = bytesToSend.Length;
            try
            {
                while (byteRemainingToSend > 0)
                {
                    byteRemainingToSend -= this.clientSocket.Send(bytesToSend);
                }
            }
            catch (SocketException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            String text = chatSendBox.Text;
            chatSendBox.Text = "";
            sendData(text, Purpose.TextMessage, "Broadcast");

        }

        private void disconnectButton_Click(object sender, EventArgs e)
        {
            disconnect = true;
            Application.Exit();

        }

        private void createEventButton_Click(object sender, EventArgs e)
        {
            if (!organizationInfoTextBox.Text.Equals(""))
            {
                String organizationInformation = organizationInfoTextBox.Text;
                organizationInfoTextBox.Clear();
                Organization organization = new Organization(organizationInformation, this.username);
                DataWrapper dataToSend = new DataWrapper("I want to create an Event", this.username, Purpose.CreateOrganization, "Broadcast");
                dataToSend.organization = organization;
                sendData(dataToSend);
                this.eventDetailsBox.Clear();
            }
            else
            {
                LogTextEvent(logTextBox, "You can't create an empty event");
            }

        }

        private void eventsCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {


            updateComboBox();


        }
        /// <summary>
        /// Updates the combobox when needed,mostly used in single organization update
        /// </summary>
        private void updateComboBox()
        {

            String currentItem = "";
            this.Invoke((MethodInvoker)delegate
            {
                if (this.eventsCombobox.SelectedItem != null)
                {
                    currentItem = this.eventsCombobox.SelectedItem.ToString();
                }
                else
                {
                    currentItem = "-1";
                }
            });

            if (!(currentItem.Equals("-1")))
            {
                int firstInteger = this.getFirstInteger(currentItem);
                selectedOrganizationId = firstInteger;
                Organization organization = organizations[firstInteger];
                this.Invoke((MethodInvoker)delegate
                {
                    this.eventDetailsBox.Text = organization.information + Environment.NewLine;
                });

                printParticipantsOfAnOrganization(organization);
            }

        }
        /// <summary>
        /// Updates combobox completely , used in batch updates
        /// </summary>
        private void updateComboBoxCompletely()
        {
            this.Invoke((MethodInvoker)delegate
            {
                this.eventsCombobox.SelectedIndex = -1;
                this.eventsCombobox.Items.Clear();
            });


            foreach (KeyValuePair<int, Organization> entry in organizations)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    this.eventsCombobox.Items.Add(entry.Value.id.ToString() + " ) " + entry.Value.information);
                });

            }

        }
        /// <summary>
        /// Gets the first integer from a text
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private int getFirstInteger(String str)
        {
            String pattern = "([0-9]+).*";
            Match match = Regex.Match(str, pattern);
            if (match.Success)
            {
                String num = match.Groups[1].Value;
                return Convert.ToInt32(num);
            }
            return -1;
        }

        private void participateButton_Click(object sender, EventArgs e)
        {
            if (this.eventsCombobox.SelectedItem != null)
            {
                Organization participatedOrganization = organizations[selectedOrganizationId];
                DataWrapper dataWrapper = new DataWrapper(participatedOrganization.id.ToString(), this.username, Purpose.AcceptOrganization, "Server");
                dataWrapper.organization = participatedOrganization;
                sendData(dataWrapper);
            }
            else
            {
                LogTextEvent(logTextBox, "There is no organization");
            }
        }

        private void rejectButton_Click(object sender, EventArgs e)
        {
            if (this.eventsCombobox.SelectedItem != null)
            {
                Organization participatedOrganization = organizations[selectedOrganizationId];
                DataWrapper dataWrapper = new DataWrapper(participatedOrganization.id.ToString(), this.username, Purpose.RejectOrganization, "Server");
                dataWrapper.organization = participatedOrganization;
                sendData(dataWrapper);
            }
            else
            {
                LogTextEvent(logTextBox, "There is no organization");
            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            if (this.eventsCombobox.SelectedItem != null)
            {
                Organization participatedOrganization = organizations[selectedOrganizationId];
                DataWrapper dataWrapper = new DataWrapper(participatedOrganization.id.ToString(), this.username, Purpose.WantUpdate, "Server");
                dataWrapper.organization = participatedOrganization;
                sendData(dataWrapper);
            }
            else
            {
                LogTextEvent(logTextBox, "There is no organization");
            }
        }

        private void getAllEventsButton_Click(object sender, EventArgs e)
        {

            getAllEvents();
        }
        private void getAllEvents()
        {


            organizations.Clear();

            this.Invoke((MethodInvoker)delegate
            {
                eventsCombobox.Items.Clear();
                eventDetailsBox.Clear();
            });


            DataWrapper dataWrapper = new DataWrapper("I want all of the updated Events", this.username, Purpose.BatchUpdate, "Server");
            sendData(dataWrapper);
        }

        private void addToBuddiesBtn_Click(object sender, EventArgs e)
        {

            if (this.onlineComboBox.SelectedItem != null)
            {
                String selectedUser = "";
                this.Invoke((MethodInvoker)delegate
                {
                    selectedUser = this.onlineComboBox.SelectedItem.ToString();
                });
                if (buddies.Contains(selectedUser) == false)
                {
                    DataWrapper dataWrapper = new DataWrapper();
                    dataWrapper.sendername = this.username;
                    dataWrapper.purpose = Purpose.WantToBeBuddies;
                    dataWrapper.Text = "I want to be buddies";
                    dataWrapper.recipient = selectedUser;
                    sendData(dataWrapper);
                }
                else
                {
                    LogTextEvent(logTextBox, Environment.NewLine + "You are already buddies with " + selectedUser + Environment.NewLine);
                }
            }
            else
            {
                LogTextEvent(logTextBox, "You can't add a empty person");
            }
        }

        private void acceptBuddyBtn_Click(object sender, EventArgs e)
        {
            if (this.buddyInvitationsComboBox.SelectedItem != null)
            {
                String selectedUser = this.buddyInvitationsComboBox.SelectedItem.ToString();
                DataWrapper dataWrapper = new DataWrapper();
                dataWrapper.sendername = this.username;
                dataWrapper.purpose = Purpose.YepWantToBeBuddies;
                dataWrapper.Text = "I want to be buddies";
                dataWrapper.recipient = selectedUser;
                removeComboBoxItem();
                sendData(dataWrapper);
            }
            else
            {
                LogTextEvent(logTextBox, "You can't add a empty person");
            }

        }

        /// <summary>
        /// Removes all the items from the combobox , in this case buddyInvitations
        /// </summary>
        private void removeComboBoxItem()
        {
            int itemsCount = -20;
            this.Invoke((MethodInvoker)delegate
            {
                itemsCount = this.buddyInvitationsComboBox.Items.Count;
            });
            if (itemsCount > 1)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    String selectedUser = this.buddyInvitationsComboBox.SelectedItem.ToString();
                    this.buddyInvitationsComboBox.Items.Remove(this.buddyInvitationsComboBox.SelectedItem);
                });

            }
            else
            {
                this.Invoke((MethodInvoker)delegate
                {
                    this.buddyInvitationsComboBox.SelectedIndex = -1;
                    this.buddyInvitationsComboBox.Items.Clear();
                });



            }
        }
        //From internet
        public void LogTextEvent(RichTextBox TextEventLog, string EventText)
        {
            if (TextEventLog.InvokeRequired)
            {
                TextEventLog.BeginInvoke(new Action(delegate
                {
                    LogTextEvent(TextEventLog, EventText);
                }));
                return;
            }

            string empty = "";



            // newline if first line, append if else.
            if (TextEventLog.Lines.Length == 0)
            {
                TextEventLog.AppendText(empty + EventText);
                TextEventLog.ScrollToCaret();
                TextEventLog.AppendText(System.Environment.NewLine);
            }
            else
            {
                TextEventLog.AppendText(empty + EventText + System.Environment.NewLine);
                TextEventLog.ScrollToCaret();
            }
        }
        private void rejectBuddyBtn_Click(object sender, EventArgs e)
        {
            if (this.buddyInvitationsComboBox.SelectedItem != null)
            {
                removeComboBoxItem();
            }
            else
            {
                LogTextEvent(logTextBox, "You can't reject a empty person");
            }
        }

    }
}
