﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;


namespace Client
{
    public static class SimpleSerializer
    {
        /// <summary>
        /// Converts a dataWrapper to a byte(still using JSON representation) array
        /// </summary>
        /// <param name="dataWrapper"></param>
        /// <returns></returns>
        public static Byte[] serializerIntoByteArray(DataWrapper dataWrapper)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            String json = addCipher(js.Serialize(dataWrapper));
            byte[] bytes = Encoding.Default.GetBytes(json);
            return bytes;
            
        }
        /// <summary>
        /// Appends the so called cipher to the end of the json data
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static String addCipher(String json)
        {

            return json + ConfigurationSettings.getHash();
        }
        /// <summary>
        /// Used to Gets the JSON part of the data
        /// </summary>
        /// <param name="str"></param>
        /// <param name="cipher"></param>
        /// <returns></returns>
        public static String getUntilCipher(String str, String cipher)
        {
            int indexOfCipher = str.IndexOf(cipher);
            return str.Substring(0, indexOfCipher);
        }
        /// <summary>
        /// NOT USED however could be an alternative
        /// </summary>
        /// <param name="str"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public static String getUntilStr(String str, String s)
        {
            int lastIndexOf = str.LastIndexOf(s);
            return str.Substring(0, lastIndexOf+1);
        }
        /// <summary>
        /// to make sure to have a complete data
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool dataIsComplete(String str)
        {

            if (str.Contains(ConfigurationSettings.getHash()))
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// NOT USED however could be an alternative
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool dataIsCompleteRegex(String str)
        {
            String pattern = @"(.*)(?!"")}(?!"")($|(\x00)+)";
            if (Regex.IsMatch(str, pattern))
            {
                return true;
            }
            return false;
        }
    }
}
