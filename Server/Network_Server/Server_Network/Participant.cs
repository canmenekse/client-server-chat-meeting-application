﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server_Network
{
    public class Participant
    {
        public String name { get; set; }
        public Status status { get; set; }

        public Participant()
        {

        }
        public Participant(String name, Status status)
        {
            this.name = name;
            this.status = status;
        }

    }

    public enum Status { NotAnswered, Rejected, Accepted };
}
