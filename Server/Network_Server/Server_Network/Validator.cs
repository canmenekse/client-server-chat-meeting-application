﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server_Network
{
    public static class Validator
    {
        public static Boolean isValidPort(String portStr)
        {
            int port;
            bool valid = false;
            if (int.TryParse(portStr, out port))
            {
                if (port < 49151)
                {
                    valid = true;
                    return valid;
                }

            }
            return valid;
        }
    }
}
