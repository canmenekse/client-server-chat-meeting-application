﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class Organization
    {
        public  int id { get; set; }
        public String information { get; set; }
        public String sender { get; set; }
        public List<Participant> participants { get; set; }
        public Organization()
        {
            
        }
        public Organization(String information , String organizer)
        {
            this.participants = new List<Participant>();
            Participant participant = new Participant(organizer, Status.NotAnswered);
            this.participants.Add(participant);
            this.information = information;
            this.sender = organizer;
            this.id = -1;
        }
        public void updateParticipantStatus(String participantName, Status status)
        {
            foreach (Participant participant in participants)
            {
                if (participantName.Equals(status))
                {
                    participant.status = status;
                }
            }
        }
        public void addParticipantIfDoesNotExist(String participantName)
        {   
                if(participantExist(participantName)==false)
                {
                    Participant participant = new Participant(participantName,Status.NotAnswered);
                    this.participants.Add(participant);
                }
               
        }
        public bool participantExist (String participantName)
        {
                foreach(Participant participant in this.participants)
                {
                    if(participant.name.Equals(participantName))
                    {
                        return true;
                    }
                }
                return false;
        }
    }
}
