﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{

    [Serializable]
    public class DataWrapper
    {
        public List<Organization> organizations { get; set; }
        public List<String> users { get; set; }
        public String Text { get; set; }
        public String sendername { get; set; }
        public Purpose purpose { get; set; }
        public String recipient { get; set; }
        public Organization organization { get; set; }

        public DataWrapper()
        {
            this.users = new List<string>();
            
        }
        public DataWrapper(String Text, String sendername, Purpose purpose, String recipient)
        {
            this.Text = Text;
            this.sendername = sendername;
            this.purpose = purpose;
            this.recipient = recipient;
            this.users = new List<string>();
            this.organizations = new List<Organization>();
            
        }
        public DataWrapper(String Text, String sendername, Purpose purpose, String recipient, Organization organization)
        {
            this.Text = Text;
            this.sendername = sendername;
            this.purpose = purpose;
            this.recipient = recipient;
            this.organization = organization;
            this.users = new List<string>();
            this.organizations = new List<Organization>();
            
        }
    }
    public enum Purpose
    {
        Auth,
        TextMessage,
        Ban,
        CreateOrganization,
        NotifyNewCreatedOrganization,
        AcceptOrganization, //5
        RejectOrganization,
        NotifyOrganizationUpdate,
        WantUpdate,
        BatchUpdate,
        sendingOnlineUsers, //10
        WantToBeBuddies,
        YepWantToBeBuddies,
        NoNotWantToBeBuddies,
        BuddyRequest, //14
        oldBuddyList,
        NotificationBatchUpdate
    };
}

