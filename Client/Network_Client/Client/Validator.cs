﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public  class Validator
    {
        private int[] errHandling;
        private String [] errStr;

        public Validator()
        {
            initializeErrStr();
            initializeErrHandling();

        }

        private void initializeErrHandling()
        {
            errHandling = new int[3];
            for (int i = 0; i < 3; i++)
            {
                errHandling[i] = 0;
            }
        }
        private void initializeErrStr()
        {
            errStr = new String[3];
            errStr[0] = " The IP Adress is invalid ";
            errStr[1] = " The Port Number is invalid ";
            errStr[2] = " The Username is empty ";
        }
        public Boolean containsErrs()
        {
            for (int i = 0; i < errHandling.Length; i++)
            {
                if (errHandling[i] == 1)
                {
                    return true;
                }
            }
            return false;
        }

        public Boolean validatedInputs(String ipAdress, String portNumber, String username)
        {
            detectIPValidity(ipAdress);
            detectPortValidity(portNumber);
            detectEmptyName(username);
            if (containsErrs())
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public String generateTheErrString()
        {
            String err="";
            for(int i=0;i<errHandling.Length;i++)
            {
                if(errHandling[i]==1)
                {
                    err+= errStr[i] +Environment.NewLine;
                }
            }
            return err;
        }

        
        
        
        
        private  void detectPortValidity(String portStr)
        {
            int port;
            bool valid = false;
            if (int.TryParse(portStr, out port))
            {
                if (port > 49151 || port <= 0)
                {
                    errHandling[1] = 1;
                }

            }
            else
            {
                errHandling[1] = 1;
            }
            
        }
        private  void detectIPValidity(String ipStr)
        {
            IPAddress ipAdress;
            bool isValidIpAddress = false;
            if (getCharacterCount('.', ipStr) == 3)
            {
                isValidIpAddress = !String.IsNullOrEmpty(ipStr) && IPAddress.TryParse(ipStr, out ipAdress);
                if (isValidIpAddress == false)
                {
                    errHandling[0] = 1;
                }
            }
            else
            {
                errHandling[0] = 1;
            }
        }
        public void detectEmptyName(String name)
        {
            if (name == "")
            {
                errHandling[2] = 1;
            }
        }
      

        public static int getCharacterCount(char character, String str)
        {
            int count = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (str.ElementAt(i) == character)
                {
                    count++;
                }
            }
            return count;
        }
    }
    

}
